
# Scenario: Pipeline must succeed

### Screenshots from MRs

1. Pipeline underway

| Image |
|-------|
|![Screen_Shot_2021-05-13_at_11.38.45_AM](/uploads/9d70e4dedf31223610d60f94cc486ebe/Screen_Shot_2021-05-13_at_11.38.45_AM.png)|

2. Pipeline passed

| Image |
|-------|
|![Screen_Shot_2021-05-13_at_11.47.51_AM](/uploads/760b72123c3f1e772fa8eb03bc529e98/Screen_Shot_2021-05-13_at_11.47.51_AM.png)|

3. Pipeline failed

| Image |
|-------|
|![Screen_Shot_2021-05-13_at_11.53.17_AM](/uploads/f2dbb6241bc7e89fca91da42362cf029/Screen_Shot_2021-05-13_at_11.53.17_AM.png)|

4. Pipeline skipped

| Image |
|-------|
|![Screen_Shot_2021-05-13_at_11.59.50_AM](/uploads/b3a9c21571dbf231143f0ffa8b6122e1/Screen_Shot_2021-05-13_at_11.59.50_AM.png)|





<addin. g info to readme to create an MR>
